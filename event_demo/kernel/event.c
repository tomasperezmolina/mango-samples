/* 
 *
 * Test application. This application
 * will launch a kernel in PEAK. The kernel will just
 * compute a matrix multiplication. The size of the matrix 
 * and the matrices pointers will be passed as parameters
 *
 * After executing the kernel, the application shows
 * the resulting output matrix
 *
*/
#include "dev/mango_hn.h"
#include "dev/debug.h"
#include <stdlib.h>


#pragma mango_kernel
void kernel_function(int16_t* B_in, int16_t* B_out, mango_event_t E_in, mango_event_t E_out, int size) {

	for (int i=0; i < 10; i++) {
		printf("signal\tE_in\tWRITE\n");
		mango_write_synchronization(&E_in, WRITE);

		printf("wait\tE_out\tWRITE\n");
		mango_wait(&E_out, WRITE);
		printf("wait\tE_in\tREAD\n");
		mango_wait(&E_in, READ);

		for (int j=0; j < size; j++) {
			B_out[j] = B_in[j] * B_in[j];
		}

		printf("signal\tE_out\tREAD\n");
		mango_write_synchronization(&E_out, READ);

	}

	return;
}

