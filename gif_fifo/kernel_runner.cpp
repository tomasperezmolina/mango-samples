#include "kernel_runner.h"
#include "host/logger.h"
#include <string>

KernelRunner::KernelRunner(int SX, int SY)
{
    this->SX=SX;
    this->SY=SY;

    // Initialization
    mango::mango_init_logger();
    // Initialization
    mango_rt = new mango::BBQContext("gif_animation", "gif_animation");

    const char *mango_root = getenv("MANGO_ROOT");
    std::string kf_path;

    if (mango_root == NULL) {
        kf_path = "/opt/mango";
    } else {
        kf_path = mango_root;
    }


#ifdef GNEMU
  char scale_kernel_file[] = "/usr/local/share/gif_fifo/scale/scale_kernel_fifo";
#else
  char scale_kernel_file[] = "/usr/local/share/gif_fifo/scale/memory.data.fpga.datafile";
#endif


#ifdef GNEMU
    auto kf_scale = new mango::KernelFunction();
    kf_scale->load(kf_path + scale_kernel_file,
                   mango::UnitType::GN, mango::FileType::BINARY);
#else
    auto kf_scale = new mango::KernelFunction();
    kf_scale->load(kf_path + scale_kernel_file,
                   mango::UnitType::PEAK, mango::FileType::BINARY);
#endif

    // Registration of task graph
    auto kscale  = mango_rt->register_kernel(KSCALE, kf_scale, {B1}, {B2});

    auto b1 = mango_rt->register_buffer(B1,
                                        SX*SY*3*sizeof(Byte), {}, {KSCALE}, mango::BufferType::FIFO );
    auto b2 = mango_rt->register_buffer(B2,
                                        SX*2*SY*2*3*sizeof(Byte), {KSCALE}, {}, mango::BufferType::FIFO );

    tg = new mango::TaskGraph({ kscale }, { b1, b2 });

    // Resource Allocation
    mango_rt->resource_allocation(*tg);

    // Execution setup
    auto argB1 = new mango::BufferArg( b1 );
    auto argB2 = new mango::BufferArg( b2 );
    auto argSX = new mango::ScalarArg<int>( SX );
    auto argSY = new mango::ScalarArg<int>( SY );
    auto argE1 = new mango::EventArg( b1->get_event() );
    auto argE2 = new mango::EventArg( b2->get_event() );

    argsKSCALE = new mango::KernelArguments(
    { argB2, argB1, argSX, argSY, argE1, argE2 }, kscale);
}

KernelRunner::~KernelRunner()
{
    // Deallocation and teardown
    mango_rt->resource_deallocation(*tg);
}

void KernelRunner::run_kernel(Byte *out, Byte *in)
{
    auto b1 = mango_rt->get_buffer(B1);
    auto b2 = mango_rt->get_buffer(B2);
    auto kscale  = mango_rt->get_kernel(KSCALE);

    // FIFO Data transfer and kernel execution
    b1->write(in, 4*SX*SY*3*sizeof(Byte));
    b2->read(out, 4*SX*2*SY*2*3*sizeof(Byte));

    auto e3=mango_rt->start_kernel(kscale, *argsKSCALE);
    e3->wait();
}
